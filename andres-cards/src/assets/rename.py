import os


imgs = os.listdir('imgs')

print('imgs/'+imgs[0])

for i, filename in enumerate(imgs):
    old_filename = 'imgs/{0}'.format(filename)
    new_filename = 'imgs/img{0}.jpg'.format(i)
    
    os.rename(old_filename, new_filename)